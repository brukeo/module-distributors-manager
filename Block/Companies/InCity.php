<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Block\Companies;

class InCity extends \Magento\Framework\View\Element\Template
{

    protected \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory;
    protected \Brukeo\DistributorsManager\Helper\GetCityNameByCityCode $getCityNameByCityCode;
    protected \Brukeo\DistributorsManager\Helper\GetMultiselectAttributeOptionsText $getMultiselectAttributeOptionsText;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Brukeo\DistributorsManager\Helper\GetCityNameByCityCode $getCityNameByCityCode,
        \Brukeo\DistributorsManager\Helper\GetMultiselectAttributeOptionsText $getMultiselectAttributeOptionsText,
        array $data = []
    )
    {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->getCityNameByCityCode = $getCityNameByCityCode;
        $this->getMultiselectAttributeOptionsText = $getMultiselectAttributeOptionsText;
        parent::__construct($context, $data);
    }

    public function getProductCollection(): \Magento\Catalog\Model\ResourceModel\Product\Collection
    {
        $cityKey = $this->getCityKey();
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addFieldToFilter('attribute_set_id', 23);
        $productCollection->addAttributeToFilter('distributor_city_key', $cityKey);
        $productCollection->setOrder('name', 'ASC');

        return $productCollection;
    }

    public function getMultiselectAttributeOptionsText(\Magento\Catalog\Model\Product $product, string $attributeCode): string
    {
        return $this->getMultiselectAttributeOptionsText->execute($product, $attributeCode);
    }

    public function getCityKey(): ?string
    {
        return $this->getRequest()->getParam('city_key');
    }

    public function _prepareLayout()
    {
        $breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');

        if ($breadcrumbsBlock) {
            $breadcrumbsBlock->addCrumb(
                'home',
                [
                    'label' => __('Home'),
                    'title' => __('Home'),
                    'link' => $this->getBaseUrl()
                ]
            );
            $breadcrumbsBlock->addCrumb(
                'companies-by-city',
                [
                    'label' => __('Companies in city'),
                    'title' => __('Companies in city'),
                    'link' => $this->getUrl('firmy-w-miescie')
                ]
            );
            $cityName = $this->getCityNameByCityCode->execute();
            $breadcrumbsBlock->addCrumb(
                'city-name',
                [
                    'label' => $cityName,
                    'title' => $cityName,
                    'link' => ''
                ]
            );
        }

        return parent::_prepareLayout();
    }

}
