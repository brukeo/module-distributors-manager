<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Block\Cities;

class Index extends \Magento\Framework\View\Element\Template
{

    protected \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        array $data = []
    )
    {
        $this->productCollectionFactory = $productCollectionFactory;

        parent::__construct($context, $data);
    }

    public function getCitiesData(): array
    {
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addFieldToFilter('attribute_set_id', 23);
        $productCollection->setOrder('distributor_city_key', 'ASC');

        $result = [];
        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($productCollection as $product) {
            $cityKey = $product->getData('distributor_city_key');
            $cityName = $product->getData('distributor_city');
            $result[$cityKey] = $cityName;
        }

        return $result;
    }

    public function getCityUrl(string $cityKey): string
    {
        $url = sprintf("%s/%s", 'firmy-w-miescie', $cityKey);

        return $this->_urlBuilder->getUrl($url);
    }

    public function _prepareLayout()
    {
        $breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');

        if ($breadcrumbsBlock) {
            $breadcrumbsBlock->addCrumb(
                'home',
                [
                    'label' => __('Home'),
                    'title' => __('Home'),
                    'link' => $this->getBaseUrl()
                ]
            );
            $breadcrumbsBlock->addCrumb(
                'companies-by-city',
                [
                    'label' => __('Companies in city'),
                    'title' => __('Companies in city'),
                    'link' => ''
                ]
            );
        }

        return parent::_prepareLayout();
    }

}
