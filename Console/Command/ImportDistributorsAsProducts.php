<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Console\Command;

class ImportDistributorsAsProducts extends \Symfony\Component\Console\Command\Command
{

    protected \Magento\Framework\App\State $appState;
    protected \Brukeo\DistributorsManager\Model\ImportDistributorsAsProducts $importDistributorsAsProducts;

    public function __construct(
        \Magento\Framework\App\State $appState,
        \Brukeo\DistributorsManager\Model\ImportDistributorsAsProducts $importDistributorsAsProducts,
        string $name = null
    )
    {
        $this->appState = $appState;
        $this->importDistributorsAsProducts = $importDistributorsAsProducts;

        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this->setName(\Brukeo\DistributorsManager\Helper\Constants::IMPORT_DISTRIBUTORS_AS_PRODUCTS_COMMAND_NAME);
        $this->setDescription(\Brukeo\DistributorsManager\Helper\Constants::IMPORT_DISTRIBUTORS_AS_PRODUCTS_COMMAND_DESCRIPTION);

        parent::configure();
    }

    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ): int
    {
        $this->appState->setAreaCode('frontend');
        $this->importDistributorsAsProducts->execute();

        return 1;
    }

}
