<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Controller;

use function Symfony\Component\String\s;

class Router implements \Magento\Framework\App\RouterInterface
{

    protected \Magento\Framework\App\ActionFactory $actionFactory;
    protected \Magento\Framework\App\ResponseInterface $response;

    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Magento\Framework\App\ResponseInterface $response
    )
    {
        $this->actionFactory = $actionFactory;
        $this->response = $response;
    }

    public function match(\Magento\Framework\App\RequestInterface $request): ?\Magento\Framework\App\ActionInterface
    {
        $identifier = trim($request->getPathInfo(), '/');
        $identifierChunks = explode("/", $identifier);

        if (count($identifierChunks) == 1 && $identifierChunks[0] == 'firmy-w-miescie') {

            $request->setModuleName('distributors_manager');
            $request->setControllerName('cities');
            $request->setActionName('index');

            return $this->actionFactory->create(\Magento\Framework\App\Action\Forward::class, [
                'request' => $request
            ]);
        }

        if (count($identifierChunks) == 2 && $identifierChunks[0] == 'firmy-w-miescie') {

            $request->setModuleName('distributors_manager');
            $request->setControllerName('companies');
            $request->setActionName('incity');
            $request->setParam('city_key', $identifierChunks[1]);

            return $this->actionFactory->create(\Magento\Framework\App\Action\Forward::class, [
                'request' => $request
            ]);
        }

        return null;
    }

}
