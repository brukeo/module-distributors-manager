<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Controller\Companies;

class InCity implements \Magento\Framework\App\Action\HttpGetActionInterface
{

    protected \Magento\Framework\View\Result\PageFactory $resultPageFactory;
    protected \Brukeo\DistributorsManager\Helper\GetCityNameByCityCode $getCityNameByCityCode;

    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Brukeo\DistributorsManager\Helper\GetCityNameByCityCode $getCityNameByCityCode
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->getCityNameByCityCode = $getCityNameByCityCode;
    }

    public function execute(): \Magento\Framework\Controller\ResultInterface
    {
        $cityName = $this->getCityNameByCityCode->execute();
        $pageTitle = __("Companies in %1", $cityName);

        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set($pageTitle);

        return $resultPage;
    }

}
