<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Controller\Cities;

class Index implements \Magento\Framework\App\Action\HttpGetActionInterface
{

    protected \Magento\Framework\View\Result\PageFactory $resultPageFactory;

    public function __construct(\Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute(): \Magento\Framework\Controller\ResultInterface
    {
        $pageTitle = __("Companies in city");

        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set($pageTitle);

        return $resultPage;
    }

}
