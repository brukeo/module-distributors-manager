<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Helper;

class GetMultiselectAttributeOptionsText
{

    public function execute(\Magento\Catalog\Model\Product $product, string $attributeCode): string
    {
        $result = [];
        $attributeData = explode(',', $product->getData($attributeCode));
        foreach ($attributeData as $value) {
            $attr = $product->getResource()->getAttribute($attributeCode);
            if ($attr->usesSource()) {
                $result[] = $attr->getSource()->getOptionText($value);
            }
        }

        return implode(", ", $result);
    }

}
