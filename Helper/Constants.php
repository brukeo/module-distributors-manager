<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Helper;

class Constants
{

    const DISTRIBUTOR_ATTRIBUTE_SET_NAME = 'Dystrybutor';
    const DISTRIBUTOR_ATTRIBUTE_SET_GROUP_NAME = 'Distributor';
    const DISTRIBUTOR_CATEGORY_NAME = 'Distributors';

    const DISTRIBUTOR_ABOUT_US_ATTRIBUTE_CODE = 'distributor_about_us';
    const DISTRIBUTOR_ABOUT_US_ATTRIBUTE_LABEL = 'About us';

    const DISTRIBUTOR_EMAIL_ATTRIBUTE_CODE = 'distributor_email';
    const DISTRIBUTOR_EMAIL_ATTRIBUTE_LABEL = 'e-Mail';

    const DISTRIBUTOR_EXHIBITION_GARDEN_ATTRIBUTE_CODE = 'distributor_exhibition_garden';
    const DISTRIBUTOR_EXHIBITION_GARDEN_ATTRIBUTE_LABEL = 'Exhibition garden';

    const DISTRIBUTOR_CITY_ATTRIBUTE_CODE = 'distributor_city';
    const DISTRIBUTOR_CITY_ATTRIBUTE_LABEL = 'City';

    const DISTRIBUTOR_EXHIBITION_GARDEN_SIZE_ATTRIBUTE_CODE = 'distributor_exhibition_garden_size';
    const DISTRIBUTOR_EXHIBITION_GARDEN_SIZE_ATTRIBUTE_LABEL = 'Exhibition garden size';

    const DISTRIBUTOR_LATITUDE_ATTRIBUTE_CODE = 'distributor_latitude';
    const DISTRIBUTOR_LATITUDE_ATTRIBUTE_LABEL = 'Latitude';

    const DISTRIBUTOR_LONGITUDE_ATTRIBUTE_CODE = 'distributor_longitude';
    const DISTRIBUTOR_LONGITUDE_ATTRIBUTE_LABEL = 'Longitude';

    const DISTRIBUTOR_MANUFACTURERS_ATTRIBUTE_CODE = 'distributor_manufacturers';
    const DISTRIBUTOR_MANUFACTURERS_ATTRIBUTE_LABEL = 'Manufacturers';

    const DISTRIBUTOR_NIP_ATTRIBUTE_CODE = 'distributor_nip';
    const DISTRIBUTOR_NIP_ATTRIBUTE_LABEL = 'NIP';

    const DISTRIBUTOR_OFFER_ATTRIBUTE_CODE = 'distributor_offer';
    const DISTRIBUTOR_OFFER_ATTRIBUTE_LABEL = 'Offer';

    const DISTRIBUTOR_OPENING_HOURS_ATTRIBUTE_CODE = 'distributor_opening_hours';
    const DISTRIBUTOR_OPENING_HOURS_ATTRIBUTE_LABEL = 'Opening hours';

    const DISTRIBUTOR_POST_CODE_ATTRIBUTE_CODE = 'distributor_post_code';
    const DISTRIBUTOR_POST_CODE_ATTRIBUTE_LABEL = 'Post code';

    const DISTRIBUTOR_PRODUCT_GROUPS_ATTRIBUTE_CODE = 'distributor_product_groups';
    const DISTRIBUTOR_PRODUCT_GROUPS_ATTRIBUTE_LABEL = 'Product groups';

    const DISTRIBUTOR_STREET_ATTRIBUTE_CODE = 'distributor_street';
    const DISTRIBUTOR_STREET_ATTRIBUTE_LABEL = 'Street';

    const DISTRIBUTOR_TELEPHONE_ATTRIBUTE_CODE = 'distributor_telephone';
    const DISTRIBUTOR_TELEPHONE_ATTRIBUTE_LABEL = 'Telephone';

    const DISTRIBUTOR_TELEPHONE_2_ATTRIBUTE_CODE = 'distributor_telephone_2';
    const DISTRIBUTOR_TELEPHONE_2_ATTRIBUTE_LABEL = 'Telephone 2';

    const DISTRIBUTOR_TYPES_ATTRIBUTE_CODE = 'distributor_types';
    const DISTRIBUTOR_TYPES_ATTRIBUTE_LABEL = 'Business types';

    const DISTRIBUTOR_VOIVODESHIPS_ATTRIBUTE_CODE = 'distributor_voivodeships';
    const DISTRIBUTOR_VOIVODESHIPS_ATTRIBUTE_LABEL = 'Voivodeships';

    const IMPORT_DISTRIBUTORS_AS_PRODUCTS_COMMAND_NAME = 'brukeo:import:distributors_as_products';
    const IMPORT_DISTRIBUTORS_AS_PRODUCTS_COMMAND_DESCRIPTION = 'Import old brukeo.pl distributors as products.';

    const GOOGLE_MAPS_EMBED_PATTERN = '<div class="distributor-google-map"><iframe style="border: 0;" src="https://maps.google.it/maps?q=%s,%s&amp;output=embed" width="800" height="600" allowfullscreen="allowfullscreen" loading="lazy"></iframe></div>';

}
