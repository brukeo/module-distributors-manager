<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Helper;

class GetCityNameByCityCode
{

    protected \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory;
    protected \Magento\Framework\App\RequestInterface $request;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->request = $request;
    }

    public function execute(): ?string
    {
        $cityKey = $this->request->getParam('city_key');
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addFieldToFilter('attribute_set_id', 23);
        $productCollection->addAttributeToFilter('distributor_city_key', $cityKey);

        return $productCollection->getFirstItem()->getData('distributor_city');
    }

}
