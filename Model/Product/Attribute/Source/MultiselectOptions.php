<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model\Product\Attribute\Source;

class MultiselectOptions extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    public function getAllOptions(): array
    {
        return [];
    }

    public function getOptionText($value)
    {
        $isMultiple = false;
        if (strpos((string) $value, ',')) {
            $isMultiple = true;
            $value = explode(',', $value);
        }

        $options = $this->getAllOptions();

        if (!is_array($value)) {
            $value = [$value];
        }
        $optionsText = [];
        foreach ($options as $item) {
            if (in_array($item['value'], $value)) {
                $optionsText[] = $item['label'];
            }
        }

        if ($isMultiple) {
            return $optionsText;
        } elseif ($optionsText) {
            return $optionsText[0];
        }

        return '';
    }

}
