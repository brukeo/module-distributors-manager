<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model\Product\Attribute\Source;

class DistributorManufacturer extends \Brukeo\DistributorsManager\Model\Product\Attribute\Source\MultiselectOptions
{

    public function getAllOptions(): array
    {
        $this->_options = [
            ['value' => '1', 'label' => 'Betard'],
            ['value' => '2', 'label' => 'Bruk-Bet'],
            ['value' => '3', 'label' => 'Bruk Czyżowice'],
            ['value' => '4', 'label' => 'Drewbet'],
            ['value' => '5', 'label' => 'Drewbet - nawierzchnie'],
            ['value' => '6', 'label' => 'Drewbet - ogrodzenia'],
            ['value' => '7', 'label' => 'Jadar'],
            ['value' => '8', 'label' => 'Kost-Bet'],
            ['value' => '9', 'label' => 'Libet'],
            ['value' => '10', 'label' => 'Polbruk'],
            ['value' => '11', 'label' => 'Pozbruk'],
            ['value' => '12', 'label' => 'Semmelrock'],
            ['value' => '13', 'label' => 'Ziel-Bruk'],
            ['value' => '14', 'label' => 'Drogbruk'],
            ['value' => '15', 'label' => 'Kamal'],
            ['value' => '16', 'label' => 'ZPB Kaczmarek'],
            ['value' => '17', 'label' => 'Joniec'],
            ['value' => '18', 'label' => 'Gladio'],
            ['value' => '19', 'label' => 'Vestone'],
            ['value' => '20', 'label' => 'Buszrem'],
            ['value' => '21', 'label' => 'Superbruk'],
            ['value' => '22', 'label' => 'Pebek']
        ];

        return $this->_options;
    }

    public function getOptionArray(): array
    {
        $result = [];
        foreach ($this->getAllOptions() as $item) {
            $result[$item['value']] = $item['label'];
        }

        return $result;
    }

}

