<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model\Product\Attribute\Source;

class DistributorType extends \Brukeo\DistributorsManager\Model\Product\Attribute\Source\MultiselectOptions
{

    public function getAllOptions(): array
    {
        $this->_options = [
            ['value' => '1', 'label' => 'Architekt'],
            ['value' => '2', 'label' => 'Dystrybutor'],
            ['value' => '3', 'label' => 'Wykonawca']
        ];

        return $this->_options;
    }

    public function getOptionArray(): array
    {
        $result = [];
        foreach ($this->getAllOptions() as $item) {
            $result[$item['value']] = $item['label'];
        }

        return $result;
    }

}

