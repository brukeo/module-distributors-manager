<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model\Product\Attribute\Source;

class DistributorProductGroup extends \Brukeo\DistributorsManager\Model\Product\Attribute\Source\MultiselectOptions
{

    protected array $useableCategories = [
        'Kostka brukowa',
        'Mała architektura',
        'Obrzeża i krawężniki',
        'Ogrodzenia i murki',
        'Palisady',
        'Prefabrykaty betonowe',
        'Produkty uzupełniające',
        'Płyty tarasowe i chodnikowe',
        'Płyty wielkoformatowe',
        'Ściany i elewacje',
        'Stopnie schodowe'
    ];

    protected \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory;

    public function __construct(\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory)
    {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    public function getUsableCategoryNames(): array
    {
        return $this->useableCategories;
    }

    public function getAllOptions(): array
    {
        if (empty($this->_options)) {
            return $this->_options = $this->getCategories();
        }

        return $this->_options;
    }

    public function getOptionArray(): array
    {
        $result = [];
        foreach ($this->getAllOptions() as $item) {
            $result[$item['value']] = $item['label'];
        }

        return $result;
    }

    protected function getCategories(): array
    {
        $result = [];
        $categories =  $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('level', ['eq' => 3])
            ->setStore(0);

        /** @var \Magento\Catalog\Model\Category $category */
        foreach ($categories as $category) {
            if (!in_array($category->getName(), $this->useableCategories)) {
                continue;
            }

            $result[] = [
                'value' => $category->getId(),
                'label' => $category->getName()
            ];
        }

        return $result;
    }

}

