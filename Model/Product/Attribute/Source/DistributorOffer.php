<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model\Product\Attribute\Source;

class DistributorOffer extends \Brukeo\DistributorsManager\Model\Product\Attribute\Source\MultiselectOptions
{

    public function getAllOptions(): array
    {
        $this->_options = [
            ['value' => '1', 'label' => 'projektowanie'],
            ['value' => '2', 'label' => 'sprzedaż wyrobów betonowych'],
            ['value' => '3', 'label' => 'transport'],
            ['value' => '4', 'label' => 'układanie']
        ];

        return $this->_options;
    }

    public function getOptionArray(): array
    {
        $result = [];
        foreach ($this->getAllOptions() as $item) {
            $result[$item['value']] = $item['label'];
        }

        return $result;
    }

}

