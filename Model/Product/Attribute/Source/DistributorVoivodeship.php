<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model\Product\Attribute\Source;

class DistributorVoivodeship extends \Brukeo\DistributorsManager\Model\Product\Attribute\Source\MultiselectOptions
{

    public function getAllOptions(): array
    {
        $this->_options = [
            ['value' => '1', 'label' => 'dolnośląskie'],
            ['value' => '2', 'label' => 'kujawsko-pomorskie'],
            ['value' => '3', 'label' => 'lubelskie'],
            ['value' => '4', 'label' => 'lubuskie'],
            ['value' => '5', 'label' => 'łódzkie'],
            ['value' => '6', 'label' => 'małopolskie'],
            ['value' => '7', 'label' => 'mazowieckie'],
            ['value' => '8', 'label' => 'opolskie'],
            ['value' => '9', 'label' => 'podkarpackie'],
            ['value' => '10', 'label' => 'podlaskie'],
            ['value' => '11', 'label' => 'pomorskie'],
            ['value' => '12', 'label' => 'śląskie'],
            ['value' => '13', 'label' => 'świętokrzyskie'],
            ['value' => '14', 'label' => 'warmińsko-mazurskie'],
            ['value' => '15', 'label' => 'wielkopolskie'],
            ['value' => '16', 'label' => 'zachodniopomorskie']
        ];

        return $this->_options;
    }

    public function getOptionArray(): array
    {
        $result = [];
        foreach ($this->getAllOptions() as $item) {
            $result[$item['value']] = $item['label'];
        }

        return $result;
    }

}

