<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model;

class ImportDistributorsAsProducts
{

    protected \Magento\Framework\App\ResourceConnection $resourceConnection;
    protected \Magento\Catalog\Api\ProductRepositoryInterface $productRepository;
    protected \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory;
    protected \Brukeo\DistributorsManager\Model\Import\PrepareProductUrlKey $prepareProductUrlKey;
    protected \Brukeo\DistributorsManager\Model\Import\PrepareTelephoneData $prepareTelephoneData;
    protected \Brukeo\DistributorsManager\Model\Import\MapManufacturers $manufacturersMapper;
    protected \Brukeo\DistributorsManager\Model\Import\MapOffers $offersMapper;
    protected \Brukeo\DistributorsManager\Model\Import\MapProductGroups $productGroupsMapper;
    protected \Brukeo\DistributorsManager\Model\Import\MapTypes $typesMapper;
    protected \Brukeo\DistributorsManager\Model\Import\MapVoivodeships $voivodeshipsMapper;
    protected \Brukeo\DistributorsManager\Model\Import\GetDistributorAttributeSetId $getDistributorAttributeSetId;
    protected \Brukeo\DistributorsManager\Model\Import\GetDistributorsCategoryId $getDistributorsCategoryId;
    protected \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement;
    protected \Magento\InventoryCatalogApi\Model\SourceItemsProcessorInterface $sourceItemsProcessor;
    protected \Brukeo\DistributorsManager\Model\Import\AddImagesToMediaGallery $addImagesToMediaGallery;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory,
        \Brukeo\DistributorsManager\Model\Import\PrepareProductUrlKey $prepareProductUrlKey,
        \Brukeo\DistributorsManager\Model\Import\PrepareTelephoneData $prepareTelephoneData,
        \Brukeo\DistributorsManager\Model\Import\MapManufacturers $manufacturersMapper,
        \Brukeo\DistributorsManager\Model\Import\MapOffers $offersMapper,
        \Brukeo\DistributorsManager\Model\Import\MapProductGroups $productGroupsMapper,
        \Brukeo\DistributorsManager\Model\Import\MapTypes $typesMapper,
        \Brukeo\DistributorsManager\Model\Import\MapVoivodeships $voivodeshipsMapper,
        \Brukeo\DistributorsManager\Model\Import\GetDistributorAttributeSetId $getDistributorAttributeSetId,
        \Brukeo\DistributorsManager\Model\Import\GetDistributorsCategoryId $getDistributorsCategoryId,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement,
        \Magento\InventoryCatalogApi\Model\SourceItemsProcessorInterface $sourceItemsProcessor,
        \Brukeo\DistributorsManager\Model\Import\AddImagesToMediaGallery $addImagesToMediaGallery
    )
    {
        $this->resourceConnection = $resourceConnection;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->prepareProductUrlKey = $prepareProductUrlKey;
        $this->prepareTelephoneData = $prepareTelephoneData;
        $this->manufacturersMapper = $manufacturersMapper;
        $this->offersMapper = $offersMapper;
        $this->productGroupsMapper = $productGroupsMapper;
        $this->typesMapper = $typesMapper;
        $this->voivodeshipsMapper = $voivodeshipsMapper;
        $this->getDistributorAttributeSetId = $getDistributorAttributeSetId;
        $this->getDistributorsCategoryId = $getDistributorsCategoryId;
        $this->categoryLinkManagement = $categoryLinkManagement;
        $this->sourceItemsProcessor = $sourceItemsProcessor;
        $this->addImagesToMediaGallery = $addImagesToMediaGallery;
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        echo "Starting import process ... \n\n";
        $distributors = $this->resourceConnection->getConnection()->query("SELECT * FROM distributors WHERE name IS NOT NULL");
        $distributorsCategoryId = $this->getDistributorsCategoryId->execute();
        $distributorAttributeSetId = $this->getDistributorAttributeSetId->execute();
        $size = $distributors->rowCount();
        $i = 1;

        foreach ($distributors as $distributor) {
            $distributorId = (int) $distributor['distributor_id'];
            $name = (string) $distributor['name'];
            if (empty($name)) {
                continue;
            }

            $sku = $urlKey = $this->prepareProductUrlKey->execute($name, $distributorId);

            echo $size . "/" . $i . " - importing distributor with ID - " . $distributorId . ", into product with SKU - " . $sku . "\n";

            if (in_array($distributorId, [3535, 3534, 3515])) {
                echo " -> test distributor, we do not import them\n";
                continue;
            }

            $telephone = (string) $distributor['telephone'];
            $mobile = (string) $distributor['mobile'];
            $telephoneData = $this->prepareTelephoneData->execute($telephone, $mobile);
            $telephone1 = $telephoneData['telephone1'];
            $telephone2 = $telephoneData['telephone2'];

            $city = (string) $distributor['city'];
            $postCode = (string) $distributor['post_code'];
            $street = (string) $distributor['street'];
            $email = (string) $distributor['email'];
            $lat = (string) $distributor['lat'];
            $lng = (string) $distributor['lng'];
            $garden = (string) $distributor['garden'];
            $gardenSize = (string) $distributor['garden_size'];
            $aboutUs = (string) $distributor['about_us'];
            $urlCityKey = (string) $distributor['url_city_key'];
            $openingHours = (string) $distributor['opening_hours'];
            $nip = (string) $distributor['nip'];

            $manufacturer = (!empty($distributor['manufacturer'])) ? $distributor['manufacturer'] : '';
            $manufacturerIds = $this->manufacturersMapper->execute($manufacturer);

            $distributorsOffer = (!empty($distributor['distributors_offer'])) ? $distributor['distributors_offer'] : '';
            $offerIds = $this->offersMapper->execute($distributorsOffer);
            if (empty($offerIds)) {
                $offerIds[] = 2;
            }

            $productGroup = (!empty($distributor['product_group'])) ? $distributor['product_group'] : '';
            $productGroupIds = $this->productGroupsMapper->execute($productGroup);

            $typeId = (!empty($distributor['type_id'])) ? $distributor['type_id'] : '';
            $typeIds = $this->typesMapper->execute($typeId);

            $voivodeship = (!empty($distributor['voivodeship'])) ? $distributor['voivodeship'] : '';
            $voivodeshipIds = $this->voivodeshipsMapper->execute($voivodeship);

            try {
                $product = $this->productRepository->get($sku);
                $newProduct = false;
            } catch (\Magento\Framework\Exception\NoSuchEntityException $noSuchEntityException) {
                $product = $this->productFactory->create();
                $newProduct = true;
            }

            $product->setAttributeSetId($distributorAttributeSetId);
            $product->setName($name);
            $product->setSku($sku);
            $product->setUrlKey($urlKey);
            $product->setWebsiteIds([0, 1]);
            $product->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
            if ($distributorId == 3312) {
                $product->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_SEARCH);
            }
            $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
            $product->setPrice(999);

            if ($lat && $lng) {
                $description = sprintf(\Brukeo\DistributorsManager\Helper\Constants::GOOGLE_MAPS_EMBED_PATTERN, $lat, $lng);
                if ($aboutUs) {
                    $description = sprintf("%s<br><hr><br><div class='distributor-about-us'>%s</div>", $description, $aboutUs);
                }
                $product->setCustomAttribute('description', $description);
            }
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_EMAIL_ATTRIBUTE_CODE,
                $email
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_EXHIBITION_GARDEN_ATTRIBUTE_CODE,
                $garden
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_EXHIBITION_GARDEN_SIZE_ATTRIBUTE_CODE,
                $gardenSize
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_CITY_ATTRIBUTE_CODE,
                $city
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_LATITUDE_ATTRIBUTE_CODE,
                $lat
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_LONGITUDE_ATTRIBUTE_CODE,
                $lng
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_MANUFACTURERS_ATTRIBUTE_CODE,
                $manufacturerIds
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_NIP_ATTRIBUTE_CODE,
                $nip
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_OFFER_ATTRIBUTE_CODE,
                $offerIds
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_OPENING_HOURS_ATTRIBUTE_CODE,
                $openingHours
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_POST_CODE_ATTRIBUTE_CODE,
                $postCode
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_PRODUCT_GROUPS_ATTRIBUTE_CODE,
                $productGroupIds
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_STREET_ATTRIBUTE_CODE,
                $street
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_TELEPHONE_ATTRIBUTE_CODE,
                $telephone1
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_TELEPHONE_2_ATTRIBUTE_CODE,
                $telephone2
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_TYPES_ATTRIBUTE_CODE,
                $typeIds
            );
            $product->setCustomAttribute(
                \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_VOIVODESHIPS_ATTRIBUTE_CODE,
                $voivodeshipIds
            );

            if ($newProduct == true) {
                $product = $this->addImagesToMediaGallery->execute($product, $distributorId);
            }

            $this->productRepository->save($product);

            $this->categoryLinkManagement->assignProductToCategories($sku, [$distributorsCategoryId]);
            $this->sourceItemsProcessor->execute($sku, [
                [
                    \Magento\InventoryApi\Api\Data\SourceItemInterface::SOURCE_CODE => 'default',
                    \Magento\InventoryApi\Api\Data\SourceItemInterface::STATUS => \Magento\InventoryApi\Api\Data\SourceItemInterface::STATUS_IN_STOCK,
                    \Magento\InventoryApi\Api\Data\SourceItemInterface::QUANTITY => 999
                ]
            ]);

            $i++;
        }

        echo "\nProcess done. \n";
    }

}
