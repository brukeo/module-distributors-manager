<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model\Import;

class GetDistributorAttributeSetId
{

    protected \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetcollectionFactory;

    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetcollectionFactory
    )
    {
        $this->attributeSetcollectionFactory = $attributeSetcollectionFactory;
    }

    public function execute()
    {
        /** @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection $attributeSets */
        $attributeSets = $this->attributeSetcollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('attribute_set_name', \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_ATTRIBUTE_SET_NAME);

        return $attributeSets->getFirstItem()->getData('attribute_set_id');
    }

}
