<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model\Import;

class GetDistributorsCategoryId
{

    protected \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory;

    public function __construct(\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory)
    {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    public function execute(): int
    {
        return $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('name', [
                'eq' => \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_CATEGORY_NAME
            ])
            ->getFirstItem()
            ->getId();
    }

}
