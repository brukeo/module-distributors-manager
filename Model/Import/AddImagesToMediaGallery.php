<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model\Import;

class AddImagesToMediaGallery
{

    protected \Magento\Framework\App\ResourceConnection $resourceConnection;
    protected \Magento\Framework\Filesystem $filesystem;
    protected \Magento\Catalog\Model\Product\Gallery\Processor $galleryProcessor;
    protected \Magento\Catalog\Model\ResourceModel\MediaImageDeleteProcessor $mediaImageDeleteProcessor;
    protected \Magento\Catalog\Model\Product $productModel;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Catalog\Model\ResourceModel\MediaImageDeleteProcessor $mediaImageDeleteProcessor,
        \Magento\Catalog\Model\Product $productModel
    )
    {
        $this->resourceConnection = $resourceConnection;
        $this->filesystem = $filesystem;
        $this->mediaImageDeleteProcessor = $mediaImageDeleteProcessor;
        $this->productModel = $productModel;
    }

    public function execute(\Magento\Catalog\Api\Data\ProductInterface $product, int $distributorId): \Magento\Catalog\Api\Data\ProductInterface
    {

        # Delete all product images
        # $productModel = $this->productModel->loadByAttribute('sku', $product->getSku());
        # $this->mediaImageDeleteProcessor->execute($product);
        # return $product;

        $mediaPath = $this->filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath();
        $distributorImages = $this->resourceConnection->getConnection()->query(
            "SELECT * FROM distributor_images WHERE distributor_id = :distributorId", [
                'distributorId' => $distributorId
        ]);

        if ($distributorImages->rowCount()) {
            foreach ($distributorImages as $distributorImage) {
                if ($distributorImage['type'] == 2) {
                    $imagePath = sprintf("%sdistributor_image/%s", $mediaPath, $distributorImage['path']);
                    $product->addImageToMediaGallery($imagePath, ['small_image', 'image', 'thumbnail'], false, false);
                } else {
                    $imagePath = sprintf("%sdistributor_image/%s", $mediaPath, $distributorImage['path']);
                    $product->addImageToMediaGallery($imagePath, [], false, false);
                }
            }
        } else {
            $imagePath = sprintf("%sdistributor_image/%s", $mediaPath, 'distributor_no_image.png');
            $product->addImageToMediaGallery($imagePath, ['small_image', 'image', 'thumbnail'], false, false);
        }

        return $product;
    }

}
