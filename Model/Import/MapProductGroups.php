<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model\Import;

class MapProductGroups
{

    protected \Brukeo\DistributorsManager\Mapper\BrukeoOldDataSource\ProductGroup $oldSource;
    protected \Brukeo\DistributorsManager\Model\Product\Attribute\Source\DistributorProductGroup $newSource;

    public function __construct(
        \Brukeo\DistributorsManager\Mapper\BrukeoOldDataSource\ProductGroup $oldSource,
        \Brukeo\DistributorsManager\Model\Product\Attribute\Source\DistributorProductGroup $newSource
    )
    {
        $this->oldSource = $oldSource;
        $this->newSource = $newSource;
    }

    public function execute(string $ids): array
    {
        if (empty($ids)) {
            return [];
        }

        $result = [];
        $ids = explode(',', $ids);
        foreach ($ids as $id) {
            $name = $this->findName($id);
            if (empty($name)) {
                continue;
            }

            $result[] = $this->findId($name);
        }

        return array_filter($result, function($a) { return ($a !== 0); });
    }

    protected function findId(string $name): int
    {
        $newSourceOptionArray = $this->newSource->getOptionArray();
        foreach ($newSourceOptionArray as $key => $value) {
            if ($name != $value) {
                continue;
            }

            return $key;
        }

        return 0;
    }

    protected function findName(int $id): string
    {
        $oldSourceOptionArray = $this->oldSource->getOptionArray();
        foreach ($oldSourceOptionArray as $key => $value) {
            if ($key != $id) {
                continue;
            }

            return $value;
        }

        return '';
    }

}
