<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model\Import;

class PrepareProductUrlKey
{

    protected \Brukeo\Core\Helper\CovertStrToSlug $covertStrToSlug;

    public function __construct(\Brukeo\Core\Helper\CovertStrToSlug $covertStrToSlug)
    {
        $this->covertStrToSlug = $covertStrToSlug;
    }

    public function execute(string $name, int $id): string
    {
        $name = $this->covertStrToSlug->execute($name);
        $name = substr($name, 0, 59);

        return sprintf("%s-%s", $name, $id);
    }

}
