<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Model\Import;

class PrepareTelephoneData
{

    public function execute(string $telephone, string $mobile): array
    {
        $telephoneChunks = explode(",", $telephone);
        if (array_key_exists('0', $telephoneChunks) && array_key_exists('1', $telephoneChunks)) {
            return [
                'telephone1' => $this->parse($telephoneChunks[0]),
                'telephone2' => $this->parse($telephoneChunks[1]),
            ];
        }

        if (!empty($telephoneChunks[0]) && !empty($mobile)) {
            return [
                'telephone1' => $this->parse($telephoneChunks[0]),
                'telephone2' => $this->parse($mobile),
            ];
        }

        if (!empty($telephoneChunks[0])) {
            return [
                'telephone1' => $this->parse($telephoneChunks[0]),
                'telephone2' => '',
            ];
        }

        if (!empty($mobile)) {
            return [
                'telephone1' => $this->parse($mobile),
                'telephone2' => '',
            ];
        }

        return [
            'telephone1' => '',
            'telephone2' => '',
        ];
    }

    protected function parse(string $str)
    {
        $str = str_replace("+", '00', $str);
        $str = str_replace([" ", "-", "_", "\r\n", "\n", "\r"], '', $str);
        $str = preg_replace('/\s+/', '', $str);
        $str = trim($str);

        $str = htmlentities($str, 0, 'utf-8');
        $str = str_replace([" ", "&nbsp;"], "", $str);

        return $str;
    }

}
