<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Setup\Patch\Data;

class CreateDistributorsCategory implements \Magento\Framework\Setup\Patch\DataPatchInterface
{

    protected \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup;
    protected \Magento\Catalog\Api\Data\CategoryInterfaceFactory $categoryFactory;
    protected \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Catalog\Api\Data\CategoryInterfaceFactory $categoryFactory,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->categoryFactory = $categoryFactory;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        /** @var \Magento\Catalog\Api\Data\CategoryInterface $category */
        $category = $this->categoryFactory->create();
        $category->setName(\Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_CATEGORY_NAME);
        $category->setParentId(2);
        $category->setIsActive(1);
        $category->setIncludeInMenu(1);

        $this->categoryRepository->save($category);

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

}
