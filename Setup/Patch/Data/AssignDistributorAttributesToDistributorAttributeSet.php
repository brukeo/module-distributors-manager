<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\DistributorsManager\Setup\Patch\Data;

class AssignDistributorAttributesToDistributorAttributeSet implements \Magento\Framework\Setup\Patch\DataPatchInterface
{

    protected array $attributeCodesToAssign = [
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_CITY_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_EMAIL_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_EXHIBITION_GARDEN_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_EXHIBITION_GARDEN_SIZE_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_LATITUDE_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_LONGITUDE_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_MANUFACTURERS_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_NIP_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_OFFER_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_OPENING_HOURS_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_POST_CODE_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_PRODUCT_GROUPS_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_STREET_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_TELEPHONE_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_TELEPHONE_2_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_TYPES_ATTRIBUTE_CODE,
        \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_VOIVODESHIPS_ATTRIBUTE_CODE,
    ];

    protected \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup;
    protected \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory;
    protected \Magento\Catalog\Model\Config $config;
    protected \Magento\Eav\Api\AttributeManagementInterface $attributeManagement;
    protected \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetcollectionFactory;
    protected \Magento\Eav\Api\AttributeGroupRepositoryInterface $attributeGroupRepository;
    protected \Magento\Eav\Api\Data\AttributeGroupInterfaceFactory $attributeGroupFactory;
    protected \Brukeo\DistributorsManager\Model\Product\Attribute\Source\DistributorProductGroup $distributorProductGroup;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Catalog\Model\Config $config,
        \Magento\Eav\Api\AttributeManagementInterface $attributeManagement,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetcollectionFactory,
        \Magento\Eav\Api\AttributeGroupRepositoryInterface $attributeGroupRepository,
        \Magento\Eav\Api\Data\AttributeGroupInterfaceFactory $attributeGroupFactory,
        \Brukeo\DistributorsManager\Model\Product\Attribute\Source\DistributorProductGroup $distributorProductGroup
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->config = $config;
        $this->attributeManagement = $attributeManagement;
        $this->attributeSetcollectionFactory = $attributeSetcollectionFactory;
        $this->attributeGroupRepository = $attributeGroupRepository;
        $this->attributeGroupFactory = $attributeGroupFactory;
        $this->distributorProductGroup = $distributorProductGroup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetId = $this->getAttributeSetId($eavSetup);

        $attributeGroup = $this->attributeGroupFactory->create();
        $attributeGroup->setAttributeSetId($attributeSetId);
        $attributeGroup->setAttributeGroupName(\Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_ATTRIBUTE_SET_GROUP_NAME);
        $group = $this->attributeGroupRepository->save($attributeGroup);

        foreach ($this->attributeCodesToAssign as $attributeCodeToAssign) {
            $this->attributeManagement->assign(
                \Magento\Catalog\Model\Product::ENTITY,
                $attributeSetId,
                $group->getAttributeGroupId(),
                $attributeCodeToAssign,
                999
            );
        }

        $this->unassignAttributes($attributeSetId);

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorCityProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorEmailProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorExhibitionGardenProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorExhibitionGardenSizeProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorLatitudeProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorLongitudeProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorManufacturersProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorNipProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorOfferProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorOpeningHoursProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorPostCodeProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorProductGroupsProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorStreetProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorTelephoneProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorTelephone2ProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorTypesProductAttribute::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AddDistributorVoivodeshipsProductAttribute::class,
        ];
    }

    protected function getAttributeSetId(\Magento\Eav\Setup\EavSetup $eavSetup): int
    {
        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        /** @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection $attributeSets */
        $attributeSets = $this->attributeSetcollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('attribute_set_name', \Brukeo\DistributorsManager\Helper\Constants::DISTRIBUTOR_ATTRIBUTE_SET_NAME)
            ->addFieldToFilter('entity_type_id', $entityTypeId);

        return $attributeSets->getFirstItem()->getData('attribute_set_id');
    }

    protected function unassignAttributes(int $attributeSetId)
    {
        /** @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection $attributeSets */
        $attributeSets = $this->attributeSetcollectionFactory->create()->addFieldToSelect('*');
        /** @var \Magento\Eav\Api\Data\AttributeSetInterface $attributeSet */
        foreach ($attributeSets as $attributeSet) {
            if (in_array($attributeSet->getAttributeSetName(), $this->distributorProductGroup->getUsableCategoryNames())) {
                foreach ($this->attributeCodesToAssign as $attributeCodeToAssign) {
                    $this->attributeManagement->unassign(
                        $attributeSet->getAttributeSetId(),
                        $attributeCodeToAssign
                    );
                }
            }
        }
    }

}
